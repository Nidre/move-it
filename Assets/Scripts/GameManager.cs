﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    enum GameState
    {
        None,
        Starting,
        Playing,
        Over
    }

    enum PlayerAction
    {
        Move,
        Send
    }

    enum Players
    {
        Player1,
        Player2
    }

    Dictionary<Players, Player> _players;
    PlayerAction _currentPlayerAction = PlayerAction.Send;

    public float perfectScore = 100;
    public float goodScore = 50;
    public float failScore = -25;

    public float tempo = 80;
    public float maxTempo = 250;
    private float _currentTempo;
    public float tempoIncreasePerStep = 5;
    public AudioSource tempoClip;
    public Text actionText;
    private float _nextTempoTime;
    private float _lastTempoTime;

    private GameState _currentState = GameState.None;

    public float minPerfectDistance;
    public float minGoodDistance;

    private GameState CurrentState
    {
        get { return _currentState; }
        set
        {
            _currentState = value;
            print("Game State : " + _currentState.ToString());
            switch (_currentState)
            {
                case GameState.None:
                    break;
                case GameState.Starting:
                    foreach (var player in _players)
                    {
                        player.Value.Score = 0;
                    }
                    _currentTempo = tempo;
                    CurrentState = GameState.Playing;
                    break;
                case GameState.Playing:
                    break;
                case GameState.Over:
                    break;
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        _players = new Dictionary<Players, Player>();
        _players.Add(Players.Player1, GameObject.Find("Player1").GetComponent<Player>());
        _players.Add(Players.Player2, GameObject.Find("Player2").GetComponent<Player>());

        _players[Players.Player1].OnMove = OnPlayer1Send;
        _players[Players.Player2].OnMove = OnPlayer2Send;

        CurrentState = GameState.Starting;
    }

    void OnPlayer1Move(int move)
    {
        print("Player 1 Move");
        if (_players[Players.Player1].TargetMove == move)
        {
            MeasureSuccess(Players.Player1);
        }
        else _players[Players.Player1].Score += failScore;

        _players[Players.Player1].IsActionTaken = true;
    }
    void OnPlayer2Move(int move)
    {
        print("Player 2 Move");
        if (_players[Players.Player2].TargetMove == move)
        {
            MeasureSuccess(Players.Player2);
        }
        else _players[Players.Player2].Score += failScore;

        _players[Players.Player2].IsActionTaken = true;
    }
    private void MeasureSuccess(Players player)
    {
        float distance = Vector3.Distance(_players[player].targetKey.position, _players[player].incomingKey.position);
        print(distance);
        if (distance < perfectScore && _players[player].incomingKey.position.y > _players[player].TargetKeyPos.y)
        {
            print("Perfect " + _currentPlayerAction);
            _players[player].Score += perfectScore;
        }
        else if (distance < goodScore && _players[player].incomingKey.position.y > _players[player].TargetKeyPos.y)
        {
            print("Perfect " + _currentPlayerAction);
            _players[player].Score += goodScore;
        }
        else
        {
            print("Perfect " + _currentPlayerAction);
            _players[player].Score += failScore;
        }
    }
    void OnPlayer1Send(int move)
    {
        print("Send");
        _players[Players.Player2].TargetMove = move;
        _players[Players.Player1].TargetMove = -1;

        MeasureSuccess(Players.Player1);

        _players[Players.Player1].IsActionTaken = true;
    }
    void OnPlayer2Send(int move)
    {
        print("Send");
        _players[Players.Player1].TargetMove = move;
        _players[Players.Player2].TargetMove = -1;

        MeasureSuccess(Players.Player2);

        _players[Players.Player2].IsActionTaken = true;
    }

    void Update()
    {
        if (_currentState == GameState.Playing)
        {
            if (_currentPlayerAction == PlayerAction.Move)
            {
                float temp = _nextTempoTime - Time.realtimeSinceStartup;
                float diff = Time.realtimeSinceStartup - _lastTempoTime;
                float perc = diff / temp;
                foreach (Player player in _players.Values)
                {
                    player.incomingKey.anchoredPosition = new Vector3(player.IncomingKeyInitPos.x, Mathf.Lerp(player.IncomingKeyInitPos.y, -player.IncomingKeyInitPos.y, perc), player.IncomingKeyInitPos.z);
                }
            }
            if (Time.realtimeSinceStartup > _nextTempoTime)
            {
                SwitchActions();
                foreach (Player player in _players.Values)
                {
                    if (_nextTempoTime > 0 && !player.IsActionTaken)
                    {
                        print(player.ToString() + " Failed to " + _currentPlayerAction);
                        player.Score += failScore;
                        if (_currentPlayerAction == PlayerAction.Send) player.TargetMove = -1;
                    }
                    player.IsActionTaken = false;
                }

                tempoClip.Play();
                _lastTempoTime = Time.realtimeSinceStartup;
                _nextTempoTime = Time.realtimeSinceStartup + (1 * (60 / tempo));

                if (_currentTempo < maxTempo) _currentTempo += tempoIncreasePerStep;
                else _currentTempo = maxTempo;

            }
        }
    }

    void SwitchActions()
    {
        switch (_currentPlayerAction)
        {
            case PlayerAction.Move:
                _currentPlayerAction = PlayerAction.Send;
                _players[Players.Player1].OnMove = OnPlayer1Send;
                _players[Players.Player2].OnMove = OnPlayer2Send;
                break;
            case PlayerAction.Send:
                _currentPlayerAction = PlayerAction.Move;
                _players[Players.Player1].OnMove = OnPlayer1Move;
                _players[Players.Player2].OnMove = OnPlayer2Move;
                break;
        }
        actionText.text = _currentPlayerAction.ToString();
    }
}
