﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [System.Serializable]
    public class Moves
    {
        public Sprite sprite;
        public KeyCode hotKey;
    }
    public RectTransform incomingKey;
    public RectTransform targetKey;
    public Image character;
    public Action<int> OnMove;
    public Text scoreText;
    private int _targetMove;

    public int TargetMove
    {
        get { return _targetMove; }
        set 
        { 
            _targetMove = value;
        }
    }
    private float _score;

    public float Score
    {
        get { return _score; }
        set 
        {
            _score = value;
            scoreText.text = _score.ToString("N0");
        }
    }
    private bool _isActionTaken;

    public bool IsActionTaken
    {
        get { return _isActionTaken; }
        set 
        {
            _isActionTaken = value;
        }
    }
    [SerializeField]
    public Moves[] moves;

    private Vector3 _incomingKeyInitPos;

    public Vector3 IncomingKeyInitPos
    {
        get { return _incomingKeyInitPos; }
    }
    private Vector3 _targetKeyPos;

    public Vector3 TargetKeyPos
    {
        get { return _targetKeyPos; }
    }

    void Awake()
    {
        StartCoroutine("Prepare");
    }

    IEnumerator Prepare()
    {
        yield return new WaitForEndOfFrame();
        _incomingKeyInitPos = incomingKey.anchoredPosition;
        _incomingKeyInitPos.y = GameObject.Find("Canvas").GetComponent<RectTransform>().rect.height * 0.5f + incomingKey.rect.height * 0.5f;
        _targetKeyPos = targetKey.anchoredPosition;
    }

    void Update()
    {
        if (!_isActionTaken)
        {
            for (int i = 0; i < moves.Length; i++)
            {
                if (Input.GetKeyDown(moves[i].hotKey))
                {
                    if (OnMove != null)
                    {
                        OnMove(i);
                    }
                }
            }
        }
    }

    public void OnKeyPadPress(int i)
    {
        if (!_isActionTaken)
        {
            if (OnMove != null)
            {
                OnMove(i);
            }
        }
    }
}
